
mostraDados(); //Atualiza a pagina quando chamada

// -------------- Start: Recarrega a página em um tempo determinado --------------
function recarregarAutomatico() {
  let timeout;
  let count = 0;

  function resetarTempo() {
    clearTimeout(timeout);
    timeout = setTimeout(recarregarPagina, 60000);
  }

  function recarregarPagina() {
    if(window.location.href !== 'http://localhost:8080/integracao_a7/?pagina=home') {
      window.location.href = 'http://localhost:8080/integracao_a7/?pagina=home';
      if (count === 0) {
        window.location.href = 'http://localhost:8080/integracao_a7/?pagina=home';
        count += 1;
        resetarTempo();
      } else {
        window.location.href = 'http://localhost:8080/integracao_a7/?pagina=home';
      }
    } else {
      window.location.href = 'http://localhost:8080/integracao_a7/?pagina=home';
      count += 1;
      resetarTempo();
    }
  }

  document.addEventListener("mousemove", resetarTempo);
  document.addEventListener("keypress", resetarTempo);
  document.addEventListener("touchstart", resetarTempo);

  resetarTempo();
}

// -------------- Start: READ do Crud --------------
function mostraDados() {
  // var url = 'https://api.eskolare-development.com/api/integrations/eskolare/orders/?status__in=payment-approved'; // DESENVOLVIMENTO
  // var authToken = '9sqr3m80ezfMBWin3UJIrWiZmqBX1OtG9JpuYXIqMhaQTrZrDe';               // DESENVOLVIMENTO
  var url = 'https://api.eskolare.com/api/integrations/eskolare/orders/?status__in=payment-approved'; // PRODUÇÃO
  var authToken = 'XbX1N51LUr4uQv4uaG3b1Ym41gYoyWp7ZjnytWx0aQ8Zo2FCyJ';   // PRODUÇÃO
  var myHeaders = new Headers();
  myHeaders.append("Authorization", `Bearer ${authToken}`);

  var requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
  };

  fetch(url, requestOptions)
    .then(response => response.json())
    .then(data => {
      var tabela = document.getElementById('results');

      data.results.forEach(function (item) {
        var row = tabela.insertRow();

        // Função para criar e preencher as células
        function createCell(value) {
          var celula = row.insertCell();
          celula.innerHTML = value;
          celula.classList.add('text-nowrap');

          if (celula.cellIndex === 5) {
            celula.style.textAlign = "right";
          }

          if (celula.cellIndex === 6) {
            celula.style.textAlign = "right";
          }
        }

        var order = item["order_number"].replace("-01", "");
        abrirPedido(row, order);
        createCell(item["customer"]["first_name"] + " " + item["customer"]["last_name"]);
        createCell(item["customer"]["document"]);
        createCell("(" + item["customer"]["phone_area_code"] + ") " + item["customer"]["phone_number"]);
        createCell(item["customer"]["email"]);
        createCell(item["order_total"]);
        createCell(formataData(item["date_created"]));
        createCell(item["status"].replace('payment-approved', 'Pagamento Aprovado'));

        // Botão para cadastrar o pedido no A7
        createCadastrarButton(row, order);

      });
    })
    .catch(error => console.log('Erro:', error));
}

// ------------ Start: Tornar num Pedido Clicável ---------
function abrirPedido(row, numPedido) {
  var pedido = row.insertCell();
  pedido.style.cursor = 'pointer';
  pedido.style.color = 'blue';
  pedido.style.textDecoration = 'underline';
  pedido.innerHTML = numPedido;

  pedido.addEventListener('click', function () {
    abrirModal(numPedido);
  });
}

// ------------ Start: Função para abrir o modal e carregar os dados ---------
function abrirModal(numPedido) {
  var numeroFormatado = numPedido + '-01';
  var modal = new bootstrap.Modal(document.getElementById('modalPedido'));
  // var url = `https://api.eskolare-development.com/api/integrations/eskolare/orders/${numeroFormatado}`; // DESENVOLVIMENTO
  // var authToken = '9sqr3m80ezfMBWin3UJIrWiZmqBX1OtG9JpuYXIqMhaQTrZrDe';                                 // DESENVOLVIMENTO
  var url = `https://api.eskolare.com/api/integrations/eskolare/orders/${numeroFormatado}`; // PRODUÇÃO
  var authToken = 'XbX1N51LUr4uQv4uaG3b1Ym41gYoyWp7ZjnytWx0aQ8Zo2FCyJ';                     // PRODUÇÃO
  var myHeaders = new Headers();
  myHeaders.append("Authorization", `Bearer ${authToken}`);

  var requestOptions = {
    method: 'GET',
    headers: myHeaders,
    redirect: 'follow'
  };

  fetch(url, requestOptions)
    .then(response => response.json())
    .then(data => {
      document.getElementById('resultPedido').textContent = data.order_number;
      document.getElementById('dataEmissao').textContent = formataData(data.date_created);
      document.getElementById('statusPedido').textContent = data.status;
      document.getElementById('nomeCliente').textContent = data.customer.first_name + ' ' + data.customer.last_name;
      document.getElementById('endEntrega').textContent = data.shipping.shipping_address_display;
      document.getElementById('bairro').textContent = data.shipping.shipping_address.line2;
      document.getElementById('cep').textContent = data.shipping.shipping_address.postcode;
      document.getElementById('celular').textContent = '(' + data.customer.phone_area_code + ')' + ' ' + data.customer.phone_number;
      document.getElementById('cidade').textContent = data.shipping.shipping_address.city;
      document.getElementById('uf').textContent = data.shipping.shipping_address.state_code;
      document.getElementById('cpf_cnpj').textContent = data.customer.document;

      var pagamento = data.payments;
      for (var i = 0; i < pagamento.length; i++) {
        var tipopag = pagamento[i];
        document.getElementById('metodoPag').textContent = tipopag.payment_method.name;
      }

      var observacao = data.shipping.carrier.label;
      document.getElementById('observacao').textContent = observacao;

      var subtotal = data.subtotal_price;
      var numFormatado = parseFloat(subtotal).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
      document.getElementById('subtotal').textContent = numFormatado;

      var acresc = data.shipping.shipping_price;
      var acrescFormatado = parseFloat(acresc).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
      document.getElementById('acrescimo').textContent = acrescFormatado;

      var desc = data.discount_amount;
      var descFormatado = parseFloat(desc).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
      document.getElementById('desconto').textContent = descFormatado;

      var valorTotal = data.order_total;
      var totalFormatado = parseFloat(valorTotal).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
      document.getElementById('total').textContent = totalFormatado;
      mostraItens(data);

    })
    .catch(error => {
      console.error('Erro ao buscar dados:', error);
    });
  modal.show();
}

// -------------- Start: tabela de itens ---------
function mostraItens(data) {
  var tabela = document.getElementById('itens');
  tabela.innerHTML = '';
  var group = data.groups;

  for (i = 0; i < group.length; i++) {
    var grupo = group[i];
    var arrayLinhas = grupo.lines;

    for (var j = 0; j < arrayLinhas.length; j++) {
      var dadosProduto = arrayLinhas[j];
      var row = tabela.insertRow();

      var emp = row.insertCell(0);
      var qtd = row.insertCell(1);
      var codProduto = row.insertCell(2);
      var descricao = row.insertCell(3);
      var fabricante = row.insertCell(4);
      var vlrUnit = row.insertCell(5);
      var acresc = row.insertCell(6);
      var desc = row.insertCell(7);
      var total = row.insertCell(8);

      emp.textContent = 5;
      qtd.textContent = dadosProduto.quantity;
      codProduto.textContent = dadosProduto.product.upc;
      descricao.textContent = dadosProduto.product.title;
      fabricante.textContent = dadosProduto.null;
      vlrUnit.textContent = dadosProduto.selling_price;
      acresc.textContent = dadosProduto.shipping_amount;
      desc.textContent = dadosProduto.discount_amount;
      total.textContent = dadosProduto.line_total;
    }
  }
}

// ------------ Start: Botão Cadastrar Pedido -------------
// function createCadastrarButton(row, pedidoNum) {
//   var cadastrar = row.insertCell();
//   cadastrar.style.textAlign = 'center';
//   var acaoCadastrar = document.createElement('button');
//   acaoCadastrar.className = 'btn btn-sm btn-success';
//   acaoCadastrar.innerHTML = 'Cadastrar';
//   acaoCadastrar.onclick = function () {
//     var numFormatado = pedidoNum + '-01';
//     enviarDadosParaPhp(numFormatado);
//   };
//   cadastrar.appendChild(acaoCadastrar);
// }
function createCadastrarButton(row, pedidoNum) {
  var cadastrar = row.insertCell();
  cadastrar.style.textAlign = 'center';
  var acaoCadastrar = document.createElement('button');
  acaoCadastrar.className = 'btn btn-sm btn-success';
  acaoCadastrar.innerHTML = 'Cadastrar';
  acaoCadastrar.onclick = function () {
    // Usando SweetAlert para confirmar o cadastro
    Swal.fire({
      title: 'Deseja cadastrar?',
      text: 'Esta ação enviará os dados para o sistema A7. Continuar?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sim, cadastrar!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        // Se o usuário confirmar, enviar dados para o A7
        var numFormatado = pedidoNum + '-01';
        enviarDadosParaPhp(numFormatado);
      }
    });
  };
  cadastrar.appendChild(acaoCadastrar);
}


// ------------- Star: Cadastra Pedido --------------
function enviarDadosParaPhp(numPedido) {
  // Obtendo a URL atual
  var urlAtual = window.location.href;

  // Se o parâmetro "numPedido" já existe na URL, remove o "numPedido"
  if (urlAtual.includes('numPedido=')) {
    urlAtual = urlAtual.replace(/(\?|&)numPedido=[^&]*/, '');
  }

  var novaUrl = urlAtual + '&numPedido=' + numPedido;
  window.location.href = novaUrl;

  // fetch(`http://localhost/integracao_a7/index.php?pagina=home&numPedido=${numPedido}`, {
  //   method: 'POST',
  //   headers: {
  //     'Content-Type': 'application/json',
  //   },
  //   body: JSON.stringify({ entrega, aluInstProd, pagamento }),
  // })
  // .then(response => response.json())
  // .then(data => {
  //   // Exibir mensagem usando SweetAlert
  //   Swal.fire({
  //     title: 'Mensagem',
  //     text: data.message,
  //     icon: 'success',
  //   });
  // })
  // .catch(error => {
  //   // Exibir mensagem de erro usando SweetAlert
  //   Swal.fire({
  //     title: 'Erro',
  //     text: 'Ocorreu um erro ao processar a solicitação.',
  //     icon: 'error',
  //   });
  // });

  // var xhr = new XMLHttpRequest();
  // xhr.onreadystatechange = function () {
  //   if (xhr.readyState === 4) {
  //     if (xhr.status === 200) {
  //       console.log('PHP Response:', xhr.responseText);
  //       // Handle the response here
  //     } else {
  //       console.error('Error:', xhr.status);
  //     }
  //   }
  // };

  // xhr.open('POST', novaUrl);
  // xhr.send(novaUrl);
}

// -------------------- Start: Formata Data -------------------
function formataData(dateString) {
  const dataCriada = new Date(dateString);
  const dia = String(dataCriada.getDate()).padStart(2, '0');
  const mes = String(dataCriada.getMonth() + 1).padStart(2, '0');
  const ano = dataCriada.getFullYear();
  return `${dia}/${mes}/${ano}`;
}

window.onload = recarregarAutomatico;
