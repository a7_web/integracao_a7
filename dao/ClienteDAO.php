<?php
class ClienteDAO extends ConexaoDAO{
  // Método para inserir o cliente no banco de dados
  public function inserirCliente($tipoCli, $deCli, $fantCli, $cnpj_cpfCli, $insc_rgCli, $ufEmisRG_IE, $cepCli, $endCli, $numCli, $complEndCli, $baiCli, $cidCli, $estCli, $codMunicipio, $dddCli, $foneCli, $emailCli, $endEnt, $comEnt, $baiEnt, $cepEnt, $cidEnt, $estEnt){
    // Passo 1: Consulta para obter o último cliente inserido
    $ultimoCliente = "SELECT MAX(cdCli) AS max_id FROM t_cli";
    $stmtObtemUltCli = sqlsrv_query($this->db->getConnection(), $ultimoCliente);

    if ($stmtObtemUltCli === false) {
      throw new Exception("Erro ao consultar o último cliente inserido: " . print_r(sqlsrv_errors(), true));
    }

    // Passo 2: Obter o ID do último cliente
    $row = sqlsrv_fetch_array($stmtObtemUltCli);
    $ultimoClienteId = $row['max_id'];

    // Passo 3: Incrementar o ID do último cliente
    $novoClienteId = $ultimoClienteId + 1;

    // Passo 4: Inserir o novo cliente com o novo ID
    $sqlInsertClient = "INSERT INTO t_cli (cdCli, oldCod, tipoCli, deCli, fantCli, endCli, baiCli, cepCli, cidCli, estCli, dddCli, cfopCli, foneCli, fone2Cli, ramalCli, faxCli, emailCli, siteCli, cnpj_cpfCli, insc_rgCli, orgaoEmisRG, dtEmisRG, ufEmisRG_IE, inscmun, inscSuframa, cndPgtoCli, portCli, precPra, endCob, baiCob, cepCob, cidCob, estCob, respCli, foneRespCli, perComResp, endEnt, comEnt, baiEnt, cepEnt, cidEnt, estEnt, prefCli, codBloq, codVenCli, usuCadCli, ObsCli1, ObsCli2, ObsCli3, ObsCli4, ObsCli5, spcpai, spcMae, dataSpc, dataSer, usuChek, msnVend, celCli, cdRamoAtividade, codMunicipio, numCli, codPais, origemCli, contaCaixa, profissao, dtNascimento, conjuge, empresaCom, CNPJCom, enderecoCom, bairroCom, cidadeCom, estCom, cepCom, foneCom, fone2Com, complEndCli, fotoCli, centroCusto, email2Cli, dt_cad_alt, cod_vl_dif, sexo, natural, nacional, pai, mae, est_civil, setor_com, dt_spc, dt_serasa, conf_cli, cod_cartorio, cod_conv, qtd_sal_min, dia_vencto, cli_mobile, codCidade) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '', ?, '', '', '', ?, '', ?, ?, '', '', ?, '', '', 0, 0, 2, '', '', '', '', '', '', '', 0, ?, ?, ?, ?, ?, ?, '', 2, 0, 'ECOMMERCE', '', '', '', '', '', '', '', '', '', '', '', '', 0, ?, ?, 1058, 'I', '', '', '', '', '', '', '', '', '', '', '', '', '', ?, '', '', '', GETDATE(), 0, '', '', 'BRASILEIRA', '', '', '', '', '', '', 'S', 0, 0, 0, 0, 'N', ?)";
    $paramsInsert = array($novoClienteId, $novoClienteId, $tipoCli, $deCli, $fantCli, $endCli, $baiCli, $cepCli, $cidCli, $estCli, $dddCli, $foneCli, $emailCli, $cnpj_cpfCli, $insc_rgCli, $ufEmisRG_IE, $endEnt, $comEnt, $baiEnt, $cepEnt, $cidEnt, $estEnt, $codMunicipio, $numCli, $complEndCli, $codMunicipio);
    $stmtInsertClient = sqlsrv_prepare($this->db->getConnection(), $sqlInsertClient, $paramsInsert);

    if (sqlsrv_execute($stmtInsertClient) === false) {
      // Lança uma exceção em caso de erro
      throw new Exception("Erro ao inserir o cliente: " . print_r(sqlsrv_errors(), true));
    } else {
      // Cliente inserido com sucesso
      return $novoClienteId;
      //echo "<br>Cliente inserido com sucesso. ID do cliente: " . $novoClienteId;
    }
  }

  // Método para selecionar e retornar o cliente do banco de dados
  public function selecionarCliente($cliCpf){
    $sql = "SELECT * FROM t_cli WHERE cnpj_cpfCli = ?";
    $params = array($cliCpf);

    $stmt = sqlsrv_query($this->db->getConnection(), $sql, $params);

    if ($stmt === false) {
      // Lança uma exceção em caso de erro
      throw new Exception(print_r(sqlsrv_errors(), true));
    }

    $clientes = array();

    while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
      $clientes[] = $row;
    }
    return $clientes;
  }

  // Método para verificar se o cliente existe no banco de dados e retornar true ou false
  public function verificarSeClienteExiste($cliCpf){
    $sql = "SELECT COUNT(*) AS count, cdCli, deCli FROM t_cli WHERE cnpj_cpfCli = ? GROUP BY cdCli, deCli";
    $params = array($cliCpf);

    $stmt = sqlsrv_query($this->db->getConnection(), $sql, $params);

    if ($stmt === false) {
      // Lança uma exceção em caso de erro
      throw new Exception(print_r(sqlsrv_errors(), true));
    }

    $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);
    // $count = $row['count'];
    // $cdCli = $row['cdCli'];

    if ($row !== null) {
      $count = isset($row['count']) ? $row['count'] : 0;
      $cdCli = isset($row['cdCli']) ? $row['cdCli'] : null;
      $deCli = isset($row['deCli']) ? $row['deCli'] : null;
    } else {
      // Caso em que $row é nulo
      $count = 0;
      $cdCli = null;
      $deCli = null;
    }

    $clienteExiste = $count > 0;

    return array(
      'clienteExiste' => $clienteExiste,
      'cdCli' => $cdCli,
      'deCli' => $deCli
    );
  }
}