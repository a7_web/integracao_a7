<?php
    class PedidoDAO extends ConexaoDAO {
        // Método para inserir o pedido na tabela t_vendas
        public function inserirPedido($cdCli_v, $cdFpg_v, $subTotal1, $vlDesconto1, $totPro_v, $obsVen_v, $vDesc_v, $totVen_v, $cdTpg_v, $cdTabPreco, $nomeCli, $vlAcresc, $percAcresc, $qtd_item, $cod_kit, $qtd_kit) {
            $novoNrVenda = $this->obterNumeroUltimaVenda() + 1;

            $pedNum = $this->obterNumeroUltimaVendaVendedor() + 1;

            $sql = "INSERT INTO t_vendas (nrVen_v, cdemp_v, emisVen_v, cdCli_v, cdFpg_v, restringe_v, cdVen_v, nrPedCli_v, przEnt_v, tpEnt, cdMid, subTotal1, vlDesconto1, totPro_v, obsVen_v, vDesc_v, totVen_v, status_v, dtStat_v, codUsu_v, pedNum, perComissao, cdTpg_v, usuAutDesc, cdTabPreco, nomeCli, statusSep, emissPedido, codTransp, emisVen_v2, vlAcresc, percAcresc, comoChegouEmp, codEqPVMovel, codPVMovel, codVendPVMovel, centroCusto, cod_obra, qtd_item, usu_autor_venda_neg, conh_transp, tipo_transp, placa_transp, cod_mot_transp, nfe_canc, cod_unico, cod_mot_entr, prazo_entr, garantia, dt_cad_alt, cpf_cli, cod_prest, cod_kit, qtd_kit, obs_rom, pv_ecomm, status_sam, efetivou_venda_pdv) VALUES (?, 5, GETDATE(), ?, ?, 'N', 1, '', GETDATE(), 'I', 0, ?, ?, ?, ?, ?, ?, 'A', GETDATE(), 'ECOMMERCE', ?, 0, ?, '', ?, ?, 'A', GETDATE(), 0, GETDATE(), ?, ?, '', 0, 0, 0, '', 0, ?, '', 0, 'C', '', 0, 'N', '', 0, '', '', GETDATE(), '', 0, ?, ?, '', 'S', 'A', 'N')";

            $params = array($novoNrVenda, $cdCli_v, $cdFpg_v, $subTotal1, $vlDesconto1, $totPro_v, $obsVen_v, $vDesc_v, $totVen_v, $pedNum, $cdTpg_v, $cdTabPreco, $nomeCli, $vlAcresc, $percAcresc, $qtd_item, $cod_kit, $qtd_kit);

            $stmt = sqlsrv_prepare($this->db->getConnection(), $sql, $params);

            if (sqlsrv_execute($stmt) === false) {
                // Lança uma exceção em caso de erro
                throw new Exception(print_r(sqlsrv_errors(), true));
            }else{
                $this->atualizarNumVenda($novoNrVenda);
                $this->atualizarNumVendaVendedor($pedNum);

                return $novoNrVenda;
            }
        }

        // Método para inserir a venda na tabela t_vendas_kit
        public function inserirVendaKit($cod_venda, $cod_kit){
            $sql = "INSERT INTO t_vendas_kit (cod_venda, cod_kit, qtd_kit) VALUES (?, ?, 1)";

            $params = array($cod_venda, $cod_kit);

            $stmt = sqlsrv_prepare($this->db->getConnection(), $sql, $params);

            if (sqlsrv_execute($stmt) === false) {
                // Lança uma exceção em caso de erro
                throw new Exception(print_r(sqlsrv_errors(), true));
            }else{
                return true;
            }
        }

        // Método para inserir registro na tabela t_log
        public function inserirRegLog($cod_rotina, $num_doc, $obs, $ip){
            $sql = "INSERT INTO t_log (cod_usu, dt_log, cod_modulo, cod_rotina, num_doc, obs, ip) VALUES ('ECOMMERCE', GETDATE(), 1, ?, ?, ?, ?)";

            $params = array($cod_rotina, $num_doc, $obs, $ip);

            $stmt = sqlsrv_prepare($this->db->getConnection(), $sql, $params);

            if (sqlsrv_execute($stmt) === false) {
                // Lança uma exceção em caso de erro
                throw new Exception(print_r(sqlsrv_errors(), true));
            }else{
                return true;
            }
        }

        // Método para inserir registro na tabela t_itsVen
        public function inserirItsVen($nrVen_iv, $cdItem_iv, $deItem_iv, $undItem_iv, $marca_iv, $embItem_iv, $cdGru_iv, $vlCusto, $precVen_iv, $precMin_iv, $precPra_iv, $qtdeSol_iv, $qtdeAte_iv, $codCST_iv, $pesoBr_iv, $item_iv, $personalizado, $promocao, $undItem2_iv, $acresc_iv, $cdKit_iv, $barcodeIt_iv){

            $sql = "INSERT INTO t_itsVen (nrVen_iv, cdEmp_iv, cdItem_iv, deItem_iv, undItem_iv, marca_iv, locEst_iv, embItem_iv, cdGru_iv, comGru_iv, vlCusto, precVen_iv, precMin_iv, precPra_iv, qtdeSol_iv, qtdeAte_iv, qtdeEnt_iv, perDes_iv, codCST_iv, emisVen_iv, status_iv, cdVen_iv, pesoBr_iv, tipoEntrega, item_iv, personalizado, promocao, num_serie, qtdeOrig, reqOutraEmp, codUsuReq, troca, qtd_troca, usu_autor_venda_neg, comprim_producao, largura_producao, peso_producao, comprim_venda, largura_venda, unid_dim, tipo_compos, qtd_peca_producao, obs, vl_venda_producao, vl_venda_venda, qtd_producao, tipo_calc, ver_alt, separa, undItem2_iv, nao_alt_entr_nf, num_conf_entr, cod_grupo_gar, tempo_gar, vl_gar, num_bilh_gar, vl_custo_esp, vl_custo_pis, vl_custo_icms, vl_custo_fin, acresc_iv, cdKit_iv, qtd_encom_iv, barcodeIt_iv, endEst_iv, codOrig) VALUES (?, 5, ?, ?, ?, ?, 0, ?, ?, 0, ?, ?, ?, ?, ?, ?, 0, 0, ?, GETDATE(), 'A', 1, ?, 'Imediata', ?, ?, ?, '', 0, 'N', '', 'N', 0, '', 0, 0, 0, 0, 0, '', 'S', 1, '', 0, 0, 0, '', 'N', 'S', ?, 'N', 0, 0, 0, 0, 0, 0, 0, 0, 0, ?, ?, 0, ?, '', 0); SELECT SCOPE_IDENTITY() AS registro";

            $params = array($nrVen_iv, $cdItem_iv, $deItem_iv, $undItem_iv, $marca_iv, $embItem_iv, $cdGru_iv, $vlCusto, $precVen_iv, $precMin_iv, $precPra_iv, $qtdeSol_iv, $qtdeAte_iv, $codCST_iv, $pesoBr_iv, $item_iv, $personalizado, $promocao, $undItem2_iv, $acresc_iv, $cdKit_iv, $barcodeIt_iv);

            $stmt = sqlsrv_query($this->db->getConnection(), $sql, $params);

            if ($stmt === false) {
                // Lança uma exceção em caso de erro
                throw new Exception(print_r(sqlsrv_errors(), true));
            }else{
                // Obtém o ID gerado automaticamente
                sqlsrv_next_result($stmt);
                sqlsrv_fetch($stmt);

                $registroItsVen = sqlsrv_get_field($stmt, 0);
                
                return $registroItsVen;
            }
        }

        // Método para inserir registro na tabela t_itsRqv
        public function inserirItsRqv($nrven_v, $cditem, $qtde, $registro_t_itsVen){
            $sql = "INSERT INTO t_itsRqv (cdemp, nrven_v, cditem, qtde, Status_v, restringe_v, tipo_compos, registro_t_itsVen) VALUES (5, ?, ?, ?, 'A', 'N', 'S', ?)";

            $params = array($nrven_v, $cditem, $qtde, $registro_t_itsVen);

            $stmt = sqlsrv_prepare($this->db->getConnection(), $sql, $params);

            if (sqlsrv_execute($stmt) === false) {
                // Lança uma exceção em caso de erro
                throw new Exception(print_r(sqlsrv_errors(), true));
            }else{
                return true;
            }
        }

        // Método para atualizar o número da ultima venda no DB do A7
        public function atualizarNumVenda($novoNumVen){
            $sqlUpdate = "UPDATE t_venda_num SET cod_pv = ?";

            $paramsUpdate = array($novoNumVen);

            $stmtUpdate = sqlsrv_prepare($this->db->getConnection(), $sqlUpdate, $paramsUpdate);

            if(sqlsrv_execute($stmtUpdate) === false){
                // Lança uma exceção em caso de erro
                throw new Exception(print_r(sqlsrv_errors(), true));
            }else{
                return true;
            }
        }

        // Método para atualizar o número de vendas do vendedor
        public function atualizarNumVendaVendedor($novoNumVen){
            $sqlUpdate = "UPDATE t_vende SET pednum = ? WHERE cdVen = 1";

            $paramsUpdate = array($novoNumVen);

            $stmtUpdate = sqlsrv_prepare($this->db->getConnection(), $sqlUpdate, $paramsUpdate);

            if(sqlsrv_execute($stmtUpdate) === false){
                // Lança uma exceção em caso de erro
                throw new Exception(print_r(sqlsrv_errors(), true));
            }else{
                return true;
            }
        }

        // Método para recuperar o número da ultima venda realizada
        public function obterNumeroUltimaVenda() {
            $sqlNrUltVen = "SELECT MAX(nrVen_v) AS max_ven FROM t_vendas";
            $stmtObtemUltVen = sqlsrv_query($this->db->getConnection(), $sqlNrUltVen);
        
            if ($stmtObtemUltVen === false) {
                throw new Exception("Erro ao consultar o número da última venda: " . print_r(sqlsrv_errors(), true));
            }
        
            $row = sqlsrv_fetch_array($stmtObtemUltVen);
            $ultimaVendaNr = $row['max_ven'];
        
            return $ultimaVendaNr;
        }      
        
        // Método para recuperar o número da ultima venda realizada por determinado vendedor
        public function obterNumeroUltimaVendaVendedor() {
            $sqlNrUltVen = "SELECT MAX(pedNum) AS max_ven FROM t_vende WHERE cdVen = 1";
            $stmtObtemUltVen = sqlsrv_query($this->db->getConnection(), $sqlNrUltVen);
        
            if ($stmtObtemUltVen === false) {
                throw new Exception("Erro ao consultar o número da última venda: " . print_r(sqlsrv_errors(), true));
            }
        
            $row = sqlsrv_fetch_array($stmtObtemUltVen);
            $ultimaVendaNr = $row['max_ven'];
        
            return $ultimaVendaNr;
        }

        // Método para recuperar o número do ultimo registro na t_itsVen
        public function obterNumUltRegItsVen() {
            $sqlNrUltReg = "SELECT MAX(registro) AS max_reg FROM t_itsVen";
            $stmtObtemUltReg = sqlsrv_query($this->db->getConnection(), $sqlNrUltReg);
        
            if ($stmtObtemUltReg === false) {
                throw new Exception("Erro ao consultar o número do último registro: " . print_r(sqlsrv_errors(), true));
            }
        
            $row = sqlsrv_fetch_array($stmtObtemUltReg);
            $ultimoReg = $row['max_reg'];
        
            return $ultimoReg;
        }
        
        //Método para verificar se o Kit existe no banco de dados, vai retornar o objeto com o kitItens
        public function verificarSeKitExiste($cnpjEsc, $refItens){
            $sql = "SELECT * FROM vw_detalhesKitItem
            WHERE ativoKit = 'S' AND cnpjEscola = ? AND referenciaItem IN (" . implode(",", array_fill(0, count($refItens), "?")) . ")";

            $params = [$cnpjEsc];
            foreach ($refItens as $item) {
                $params[] = $item;
            }

            $stmt = sqlsrv_query($this->db->getConnection(), $sql, $params);

            if ($stmt === false) {
                // Retorna um array vazio em caso de erro
                return [];
            }

            $kitItens = array();

            while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)){
                $kitItens[] = $row;
            }

            return $kitItens;
        }
    }
?>