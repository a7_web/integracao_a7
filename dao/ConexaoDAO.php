<?php
    abstract class ConexaoDAO{
        private static $instance;
        protected $db;

        public function __construct(){
            $this->db = new Database();
        }

        public static function getInstance(){
            if(self::$instance === null){
                self::$instance = new self();
            }
            return self::$instance;
        }

        public function closeConnection(){
            sqlsrv_close($this->db->getConnection());
        }
    }

    class Database {
        private $serverName = "192.168.10.60"; // Endereço do servidor SQL Server
        // private $serverName = "DEV-JAVA\SQLEXPRESS"; // Endereço do servidor SQL Server de teste (Adson)
        private $connectionOptions = array(
            "Database" => "A7_FTD", // Nome do banco de dados
            "Uid" => "a7", // Nome de usuário
            "PWD" => "sql#Hais23" // Senha do usuário
        );

        private $connection;

        public function __construct() {
            $this->connection = sqlsrv_connect($this->serverName, $this->connectionOptions);

            if ($this->connection === false) {
                // Lança uma exceção em vez de usar die()
                throw new Exception(print_r(sqlsrv_errors(), true));
            }//else {
            //     echo json_encode(['success' => 'Conexao realizada com sucesso!']);
            // }
        }

        public function getConnection() {
            return $this->connection;
        }
    }
?>
