<?php
// Esse script é responsável pelas tarefas assíncronas do projeto. Essas tarefas são: Verificação constante da tabela t_lvSai, que guarda os dados das notas fiscais do A7, até que a nota seja emitida, o envio da requisição POST com os dados da nota fiscal e a mudança de status de "Handling" para "Invoice" na plataforma da Eskolare.

// Trecho que realiza a conexão com a base de dados, independente da aplicação principal
abstract class ConexaoAgendadorDAO{
    private static $instance;
    protected $db;

    public function __construct(){
        $this->db = new DatabaseAgendador();
    }

    public static function getInstance(){
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function closeConnection(){
        sqlsrv_close($this->db->getConnection());
    }
}

class DatabaseAgendador{
    private $serverName = "192.168.10.60"; // Endereço do servidor SQL Server
    // private $serverName = "DEV-JAVA\SQLEXPRESS"; // Endereço do servidor SQL Server de teste (Adson)
    private $connectionOptions = array(
        "Database" => "A7_FTD", // Nome do banco de dados
        "Uid" => "a7", // Nome de usuário
        "PWD" => "sql#Hais23" // Senha do usuário
    );

    private $connection;

    public function __construct(){
        $this->connection = sqlsrv_connect($this->serverName, $this->connectionOptions);

        if ($this->connection === false) {
            // Lança uma exceção em vez de usar die()
            throw new Exception(print_r(sqlsrv_errors(), true));
        }
    }

    public function getConnection(){
        return $this->connection;
    }
}
// =============================================================================================================================================================

// Classe responsável pelas consultas ao SQL Server
class PedidoAgendadorDAO extends ConexaoAgendadorDAO{
    // Método responsável por verificar se há NFe emitida para o pedido
    public function verificarNfeDoPedido($numPed){
        $sql = "SELECT numNF, dtEmi, totNF, nrVen, localXML_nfe FROM t_lvSai
            WHERE cdEmp = 5 AND status = 'Ativa' AND nrVen = ? AND NFeEnviada = 'S'";
        $params = array($numPed);

        $stmt = sqlsrv_query($this->db->getConnection(), $sql, $params);

        if ($stmt === false) {
            // Lança uma exceção em caso de erro
            throw new Exception(print_r(sqlsrv_errors(), true));
        }

        $nfePedido = array();

        while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
            $nfePedido[] = $row;
        }

        return $nfePedido;
    }

    // Método para inserir registro na tabela t_log
    public function inserirRegLog($cod_rotina, $num_doc, $obs, $ip){
        $sql = "INSERT INTO t_log (cod_usu, dt_log, cod_modulo, cod_rotina, num_doc, obs, ip) VALUES ('ECOMMERCE', GETDATE(), 1, ?, ?, ?, ?)";

        $params = array($cod_rotina, $num_doc, $obs, $ip);

        $stmt = sqlsrv_prepare($this->db->getConnection(), $sql, $params);

        if (sqlsrv_execute($stmt) === false) {
            // Lança uma exceção em caso de erro
            throw new Exception(print_r(sqlsrv_errors(), true));
        } else {
            return true;
        }
    }
}
// =============================================================================================================================================================

// Trecho responsável pela execução das rotinas dinâmicas de integração
class VerificadorPedidosNfe{
    // Atributo
    private $pedidoDAO;

    // Construtor
    public function __construct(){
        $this->pedidoDAO = new PedidoAgendadorDAO();
    }

    // Métodos
    public function verificarPedidosComNota(){
        try {
            // Recupera o arquivo com o número dos pedidos inseridos no A7 pelo integrador
            $filePath = 'C:/SIAH/A7/numPedidosA7.txt';

            // Verifica se o arquivo não está vazio
            if (filesize($filePath) > 0) {
                // Ler o(s) número(s) de pedido do arquivo
                $numerosPedido = file($filePath, FILE_IGNORE_NEW_LINES);

                // Aciona o método para verificar se a nota foi emitida para o pedido
                foreach ($numerosPedido as $numero) {
                    $dadosNota = $this->pedidoDAO->verificarNfeDoPedido($numero);

                    if (!empty($dadosNota)) {
                        // Processo para enviar os dados da nota à API
                        if ($this->atualizarComDadosNfeApi($dadosNota)) {
                            // Atualiza a tabela de Logs do A7
                            $obsLog = 'Os dados da NF-e do pedido foram encaminhados com sucesso para Eskolare';
                            $ip = $_SERVER['REMOTE_ADDR'];
                            $this->pedidoDAO->inserirRegLog(33, $dadosNota[0]['nrVen'], $obsLog, $ip);

                            // Remove o número do array
                            $posicao = array_search($numero, $numerosPedido);
                            if ($posicao !== false) {
                                unset($numerosPedido[$posicao]);
                            }
                        } else {
                            error_log("Erro ao enviar dados da NF-e para a API Eskolare. Pedido: $numero");
                        }
                    }
                }

                // Reescreve o arquivo com os números de pedidos restantes
                file_put_contents($filePath, implode(PHP_EOL, $numerosPedido));
            }
        } catch (Exception $e) {
            error_log("Erro no script: " . $e->getMessage());
        }
    }

    private function atualizarComDadosNfeApi($dadosNota){
        // Atribui os respectivos dados da nota as suas variáveis locais
        $external_id = $dadosNota[0]['nrVen'];
        $invoice_number = $dadosNota[0]['numNF'];
        $dataFormatada = date('Y-m-d\TH:i:sP', $dadosNota[0]['dtEmi']->getTimestamp());
        $file = $dadosNota[0]['localXML_nfe'];
        $invoice_amount = $dadosNota[0]['totNF'];

        // Ler o conteúdo do arquivo XML
        $xmlContent = file_get_contents($file);
        // Converte XML para base64
        $xmlBase64 = base64_encode($xmlContent);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.eskolare-development.com/api/integrations/eskolare/orders/' . $external_id . '/invoice' . '/', //DESENVOLVIMENTO
            // CURLOPT_URL => 'https://api.eskolare.com/api/integrations/eskolare/orders/'.$external_id.'/invoice'.'/', // PRODUÇÃO
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                "invoice_number": "' . $invoice_number . '",
                "invoice_amount": "' . $invoice_amount . '",
                "invoice_type": "input",
                "invoice_url": "",
                "file": "' . $xmlBase64 . '",
                "issuance_date": "' . $dataFormatada . '",
                "carrier": "",
                "tracking_number": "",
                "tracking_url": "",
                "external_id": "' . $external_id . '"
            }',
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer 9sqr3m80ezfMBWin3UJIrWiZmqBX1OtG9JpuYXIqMhaQTrZrDe',
                'Content-Type: application/json',
            ), // DESENVOLVIMENTO
            // CURLOPT_HTTPHEADER => array(
            //     'Authorization: Bearer XbX1N51LUr4uQv4uaG3b1Ym41gYoyWp7ZjnytWx0aQ8Zo2FCyJ',
            //     'Content-Type: application/json',
            // ), // PRODUÇÃO
        ));

        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if ($httpCode === 200) {
            return $response;
        } else {
            return false;
        }
    }
}

// Cria uma instância da classe e chame o método para teste
$verificador = new VerificadorPedidosNfe();
$verificador->verificarPedidosComNota();

// // Abre ou cria o arquivo de log em modo de escrita ao final do arquivo
// $file = fopen('C:/xampp/htdocs/integracao_a7/log.txt', 'a');

// // Redireciona a saída padrão (stdout) para o arquivo de log
// ob_start();

// // Chama o método para verificar os pedidos
// $verificador->verificarUltimosPedidosSemNota();

// // Obtém o conteúdo do buffer de saída
// $output = ob_get_clean();

// // Escreve o conteúdo do buffer no arquivo de log
// fwrite($file, $output);

// // Fecha o arquivo de log
// fclose($file);
