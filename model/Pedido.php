<?php
class Pedido{
  // Atributos
  private $pedidoDAO;
  private $cliente;
  private $contador = 0;

  // Métodos especiais
  // Construtor
  // Cria-se uma instância da classe PedidoDAO, que é subclasse de ConexaoDAO, essa instância chama os métodos específicos de PedidoDAO
  public function __construct($cliente){
    $this->pedidoDAO = new PedidoDAO();
    $this->cliente = $cliente;
  }

  // Métodos
  // Método responsável por receber os dados da API por completo
  public function receberDadosApi($dadosPed){
    $shipping = $this->lerDadosEntrega($dadosPed);
    $aluInstProd = $this->lerDadosAlunoInstituicaoProduto($dadosPed);
    $payments = $this->lerDadosPagamento($dadosPed);

    $this->cadastrarPedido($shipping, $aluInstProd, $payments);
  }

  // Método que vai receber os dados do pedido para validá-lo no banco de dados do A7
  private function cadastrarPedido($entrega, $aluInstProd, $pagamento){
    try {
      $institution = $aluInstProd->students[0]->institution;

      $upcsProdutos = [];
      $valoresProdutos = [];

      foreach ($aluInstProd->students[0]->products as $product) {
        $upcsProdutos[] = $product->upc;
        $valoresProdutos[] = $product->selling_price;
      }

      $kitItens = $this->pedidoDAO->verificarSeKitExiste($institution, $upcsProdutos);
      // echo "kitItens: ".print_r($kitItens, true);

      if (!empty($kitItens)) {
        // Obtém os dados do kit selecionado
        $cdKit = $kitItens[0]['codigoKit'];
        $descKit = $kitItens[0]['descricaoKit'];
        $qtdItem = $kitItens[0]['qtdTotalItensKit'];
        $cdTabPreco = $kitItens[0]['tabelaPrecoKit'];

        // Obtém os dados do pedido na API
        $subTotal = $entrega->subtotal_price;
        $desconto = $entrega->discount_amount;
        $totalVenda = $entrega->order_total;
        $acrescimo = $entrega->shipping_price;
        $percAcresc = $this->calcularPorcentagemAcrescimo($acrescimo, $subTotal);
        $obsVenda = 'ESKOLARE: ' . $entrega->order_number . ' / ' . strtoupper($this->extrairObs($entrega->label)) . ' / KIT: ' . $descKit . ' / ALUNO(A): ' . strtoupper($this->removerAcentos($aluInstProd->students[0]->name)) . ' / PAGAMENTO: ' . strtoupper($this->removerAcentos($pagamento));

        // Obtém os dados do cliente
        $cdCli = $this->cliente->getCodigoCliente();
        $deCli = $this->cliente->getNomeCliente();

        // echo json_encode(['message' => 'Codigo cliente: '.$cdCli.'Nome cliente: '.$deCli]);

        // Insere a venda na tabela t_vendas e retorna o número dessa nova venda
        $nrNovaVenda = $this->pedidoDAO->inserirPedido($cdCli, 1, $subTotal, $desconto, $subTotal, $obsVenda, $desconto, $totalVenda, 1, $cdTabPreco, $deCli, $acrescimo, $percAcresc, $this->contador, $cdKit, $qtdItem);

        // Verifica se o número da nova venda foi retornado
        if (!empty($nrNovaVenda)) {
          // Insere a venda na tabela t_vendas_kit e retorna true ou false
          if ($this->pedidoDAO->inserirVendaKit($nrNovaVenda, $cdKit)) {
            $obsLog = 'Total: ' . $totalVenda . ' Vend.: 1';
            $ip = $_SERVER['REMOTE_ADDR'];
            // Insere registro na t_log
            $this->pedidoDAO->inserirRegLog(1, $nrNovaVenda, $obsLog, $ip);

            // Insere registro na t_itsVen
            $cont = 1;
            foreach ($kitItens as $index => $item) {
              $cdItem_iv = $item['codigoItem'];
              $deItem_iv = $item['descricaoItem'];
              $undItem_iv = $item['undMedItem'];
              $marca_iv = $item['marcaItem'];
              $embItem_iv = $item['qtEmbItem'];
              $cdGru_iv = $item['grupoItem'];
              $vlCusto = $item['vlrCustoItem'];
              $precVen_iv = $item['vlrVenda'];
              $precMin_iv = $item['vlrMinimo'];
              $precPra_iv = $valoresProdutos[$index];
              $qtdeSol_iv = $item['qtdItensKit'];
              $qtdeAte_iv = $item['qtdItensKit'];
              $codCST_iv = $item['cstItem'];
              $pesoBr_iv = $item['pesoItem'];
              $item_iv = $cont++;
              $personalizado = $item['personalizado'];
              $promocao = $item['promocao'];
              $undItem2_iv = $item['undMedItem'];
              $acresc_iv = $item['acresc'];
              $barcodeIt_iv = $item['codigoBarraItem'];

              $registroItsVen = $this->pedidoDAO->inserirItsVen($nrNovaVenda, $cdItem_iv, $deItem_iv, $undItem_iv, $marca_iv, $embItem_iv, $cdGru_iv, $vlCusto, $precVen_iv, $precMin_iv, $precPra_iv, $qtdeSol_iv, $qtdeAte_iv, $codCST_iv, $pesoBr_iv, $item_iv, $personalizado, $promocao, $undItem2_iv, $acresc_iv, $cdKit, $barcodeIt_iv);

              if(!empty($registroItsVen)){
                $resposta = $this->pedidoDAO->inserirItsRqv($nrNovaVenda, $cdItem_iv, $qtdeAte_iv, $registroItsVen);
              }else{
                echo '<script>Swal.fire("[4] - Não foi possível inserir a venda no sistema!");</script>';
              }
            }

            if ($resposta) {
              // Aciona o método que insere o número da venda do A7 na API
              // $this->atualizarExternalIdApi($entrega->order_number, $nrNovaVenda);
              // Aciona o método que é responsável para enviar o retorno POST para pedido em manuseio(Handling)
              // $this->atualizarStatusHandlingApi($entrega->order_number);
              // Aciona o método que salva o número da venda num arquivo de texto, para fazer as consultas automáticas se existe a NFe do pedido
              $this->salvarNumeroPedidoTxt($nrNovaVenda);
              echo
              '<script>
                Swal.fire({
                  icon: "success",
                  title: "Pedido Cadastrado",
                  html: "Pedido: <b>' . $nrNovaVenda . '</b>, cadastrado com sucesso!",
                  confirmButtonColor: "#039ddf",
                }).then(function(result) {
                  if (result.isConfirmed) {
                    window.location.href = "http://localhost:8080/integracao_a7/?pagina=home";
                  }
                });
                </script>';
            } else {
              echo '<script>Swal.fire("[5] - Não foi possível inserir a venda no sistema!");</script>';
            }
          } else {
            echo '<script>Swal.fire("[3] - Não foi possível inserir a venda no sistema!");</script>';
          }
        } else {
          echo '<script>Swal.fire("[2] - Não foi possível inserir a venda no sistema!");</script>';
        }
      } else {
        echo '<script>Swal.fire("[1] - Lamentamos informar que não foi possível encontrar nenhum kit associado a escola ou aos itens do pedido da Eskolare no Sistema A7!");</script>';
      }
    } catch (Exception $e) {
      echo json_encode(['message' => $e->getMessage()]);
    }
  }

  // Método para separar apenas os dados referentes a valores e a entrega do pedido, que serão relevantes no A7
  private function lerDadosEntrega($dadosEnt){
    if ($dadosEnt !== null) {
      // Cria um objeto vazio
      $entrega = new stdClass();

      $entrega->order_number = $dadosEnt['order_number'];
      $entrega->subtotal_price = $dadosEnt['subtotal_price'];
      $entrega->discount_amount = $dadosEnt['discount_amount'];
      $entrega->order_total = $dadosEnt['order_total'];
      $entrega->shipping_price = $dadosEnt['shipping']['shipping_price'];
      $entrega->label = $dadosEnt['shipping']['carrier']['label'];

      return $entrega;
    } else {
      return null;
    }
  }

  // Método para separar apenas os dados referentes ao nome do aluno, CNPJ da instituição e os dados do UPC(referência) e do valor total dos itens do pedido
  private function lerDadosAlunoInstituicaoProduto($dadosAluInstProd){
    if ($dadosAluInstProd !== null) {
      // Cria um objeto vazio
      $aluInstProd = new stdClass();
      $aluInstProd->students = [];

      foreach ($dadosAluInstProd['groups'] as $grupo) {
        $student = new stdClass();
        $student->name = $grupo['enrollment']['student']['first_name'] . ' ' . $grupo['enrollment']['student']['last_name'];

        $institution = $this->retirarCaracteresCnpj($grupo['enrollment']['institution']['tax_document']);
        $student->institution = $institution;

        // Agora, vamos lidar com os produtos associados a este aluno
        $student->products = [];

        foreach ($grupo['lines'] as $line) {
          $product = new stdClass();
          $product->title = $line['product']['title'];
          $product->upc = $line['product']['upc'];
          $product->quantity = $line['quantity'];
          $product->selling_price = $line['selling_price'];
          $product->line_total = $line['line_total'];

          $student->products[] = $product;

          $this->contador++;
        }

        $aluInstProd->students[] = $student;
      }

      return $aluInstProd;
    } else {
      return null;
    }
  }

  // Método para separar apenas os dados referentes ao pagamento do pedido na Eskolare, para definir tipo e condição de pagamento
  private function lerDadosPagamento($dadosPagamento)
  {
    if ($dadosPagamento !== null && isset($dadosPagamento['payments'])) {
      // Cria uma lista de pagamentos
      $listaPagamentos = [];

      foreach ($dadosPagamento['payments'] as $payment) {
        // Cria um objeto pagamento vazio
        $pagamento = new stdClass();
        $pagamento->name = $payment['payment_method']['name'];
        $pagamento->group = $payment['payment_method']['group'];
        $pagamento->installments = $payment['installments'];
        $pagamento->total = $payment['total'];

        // Adiciona o pagamento à lista
        $listaPagamentos[] = $pagamento;
      }

      // Construção da string com base no tipo e condição de pagamento da API
      $descCompleta = '';

      foreach ($listaPagamentos as $pagamento) {
        $descCompleta .= $pagamento->name;

        switch ($pagamento->group) {
          case 'bank_debit':
            $descCompleta .= ' ' . $pagamento->installments . ' DIA(S)';
            break;
          case 'billing_note':
            $descCompleta .= ' ' . $pagamento->installments . ' PARCELA(S)';
            break;
          case 'credit_card':
            $descCompleta .= ' CREDITO ' . $pagamento->installments . ' X';
            break;
          case 'pix':
            $descCompleta .= ' ' . $pagamento->installments . ' DIA(S)';
            break;
          default:
            $descCompleta .= ' ' . $pagamento->installments . ' PARCELA(S)';
        }

        // Adiciona o valor total de cada tipo de pagamento se houver mais de um pagamento
        if (count($listaPagamentos) > 1) {
          $descCompleta .= ' - VALOR R$ ' . $pagamento->total . '; ';
        }
      }

      // Retorna a string construída
      return $descCompleta;
      // echo "Descrição Completa: ".print_r($descCompleta, true);
    } else {
      return null;
    }
  }

  // Método para formatar o CNPJ para ficar somente os números
  private function retirarCaracteresCnpj($cnpjInst){
    $cnpjFormatado = str_replace(array(".", "/", "-"), "", $cnpjInst);

    return $cnpjFormatado;
  }

  // Método para calcular a % de acréscimo e inserir na venda
  private function calcularPorcentagemAcrescimo($valorAcrescimo, $valorSubTotal){
    if ($valorSubTotal == 0) {
      return 0; // Evitar divisão por zero
    }

    $porcentagemAcrescimo = ($valorAcrescimo / $valorSubTotal) * 100;

    return $porcentagemAcrescimo;
  }

  // Método para remover acentos de uma string
  private function removerAcentos($str){
    $novaStr = preg_replace(array(
      "/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/",
      "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/", "/(ç)/", "/(Ç)/"
    ), explode(" ", "a A e E i I o O u U n N c C"), $str);

    return $novaStr;
  }

  // Método para extrair uma parte da observação do pedido, onde tenha o caractere (-)
  private function extrairObs($str){
    // Encontra a posição da primeira ocorrência de (-)
    $posicao = strpos($str, " - ");

    // Se (-) for encontrado, extrai a parte antes dele
    if($posicao !== false){
      $novaStr = substr($str, 0, $posicao);
    }else{
      // Se (-) não for encontrado, a frase permanece a mesma
      $novaStr = $str;
    }

    return $this->removerAcentos($novaStr);
  }

  // Método para inserir o número da venda do A7 no external_id na API da Eskolare
  private function atualizarExternalIdApi($orderNumber, $external_id){
    $curl = curl_init();

    curl_setopt_array($curl, array(
      // CURLOPT_URL => 'https://api.eskolare-development.com/api/integrations/eskolare/orders/' . $orderNumber . '/', // DESENVOLVIMENTO
      CURLOPT_URL => 'https://api.eskolare.com/api/integrations/eskolare/orders/'.$orderNumber.'/', // PRODUÇÃO
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'PATCH',
      CURLOPT_POSTFIELDS => '{
                "external_id": "' . $external_id . '"
            }',
      // CURLOPT_HTTPHEADER => array(
      //   'Authorization: Bearer 9sqr3m80ezfMBWin3UJIrWiZmqBX1OtG9JpuYXIqMhaQTrZrDe',
      //   'Content-Type: application/json',
      // ), // DESENVOLVIMENTO
      CURLOPT_HTTPHEADER => array(
          'Authorization: Bearer XbX1N51LUr4uQv4uaG3b1Ym41gYoyWp7ZjnytWx0aQ8Zo2FCyJ',
          'Content-Type: application/json',
      ), // PRODUÇÃO
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
  }

  // Método para alterar o status para "Handling" na API da Eskolare
  private function atualizarStatusHandlingApi($orderNumber){
    $curl = curl_init();

    curl_setopt_array($curl, array(
      // CURLOPT_URL => 'https://api.eskolare-development.com/api/integrations/eskolare/orders/' . $orderNumber . '/handling', // DESENVOLVIMENTO
      CURLOPT_URL => 'https://api.eskolare.com/api/integrations/eskolare/orders/'.$orderNumber.'/handling', // PRODUÇÃO
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      // CURLOPT_HTTPHEADER => array(
      //   'Authorization: Bearer 9sqr3m80ezfMBWin3UJIrWiZmqBX1OtG9JpuYXIqMhaQTrZrDe', // DESENVOLVIMENTO
      // ),
      CURLOPT_HTTPHEADER => array(
          'Authorization: Bearer XbX1N51LUr4uQv4uaG3b1Ym41gYoyWp7ZjnytWx0aQ8Zo2FCyJ', // PRODUÇÃO
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
  }

  private function salvarNumeroPedidoTxt($numPed){
    // Abre ou cria o arquivo de log em modo de escrita ao final do arquivo
    $file = fopen('C:/SIAH/A7/numPedidosA7.txt', 'a');

    // Redireciona a saída padrão (stdout) para o arquivo de log
    ob_start();

    // Remove espaços em branco adicionais
    $numPed = trim($numPed);

    // Escreve a mensagem no arquivo de log
    fwrite($file, $numPed . PHP_EOL);

    // Obtém o conteúdo do buffer de saída
    $output = ob_get_clean();

    // Escreve o conteúdo do buffer no arquivo de log
    fwrite($file, $output);

    // Fecha o arquivo de log
    fclose($file);
  }
}
