<?php
    class Cliente{
        // Atributos
        private $clienteDAO;
        private $cdCli;
        private $deCli;

        // Métodos
        // Método responsável por receber os dados da API por completo
        public function receberDadosApi($dadosCli){
          // Obtém os dados do cliente
          $customer = $this->lerDadosCliente($dadosCli);
          $address = $this->lerEnderecoEntrega($dadosCli);
          $cliente = $this->concatenarDadosCliente($customer, $address);
          $this->cadastrarCliente($cliente);
        }

        // Método que vai receber os dados do cliente para cadastrá-lo no banco de dados do A7
        private function cadastrarCliente($cliente){
            $cliCpf = $this->removerCaracteresEspeciais($cliente['document']);
            $clienteExiste = $this->verificarClienteExistente($cliCpf);

            if($clienteExiste['clienteExiste']){
                $this->cdCli = $clienteExiste['cdCli'];
                $this->deCli = $clienteExiste['deCli'];
                // echo '<script>
                //   Swal.fire({
                //     icon: "warning",
                //     title: "Cliente já existe",
                //     html: "Cliente: <b>'.$this->cdCli.' - '.$this->deCli.'</b> já está cadastrado!<br><br>Cadastre o pedido!",
                //     confirmButtonColor: "#039ddf",
                //   });
                // </script>';
            }else{
                $tipoCli = $this->validarPF_PJ($cliente);
                $this->deCli = strtoupper($this->removerAcentos($cliente['first_name'] . ' ' . $cliente['last_name']));
                $fantCli = strtoupper($this->removerAcentos($cliente['first_name'] . ' ' . $cliente['last_name']));
                $cnpj_cpfCli = $this->removerCaracteresEspeciais($cliente['document']);
                $insc_rgCli = $this->definirIE_RG($tipoCli);
                $ufEmisRG_IE = $cliente['state_code'];
                $cepCli = $cliente['postcode'];
                $endCli = strtoupper($this->removerAcentos($cliente['line1']));
                $numCli = strtoupper($cliente['line4']);
                $complEndCli = strtoupper($this->removerAcentos($cliente['line3']));
                $baiCli = strtoupper($this->removerAcentos($cliente['line2']));
                $cidCli = strtoupper($this->removerAcentos($cliente['city']));
                $estCli = strtoupper($cliente['state_code']);
                $codMunicipio = $cliente['city_code'];
                $dddCli = $cliente['phone_area_code'];
                $foneCli = $cliente['phone_number'];
                $emailCli = $cliente['email'];
                $endEnt = strtoupper($this->removerAcentos($cliente['line1'])).', '.strtoupper($cliente['line4']);
                $comEnt = strtoupper($this->removerAcentos($cliente['line3']));
                $baiEnt = strtoupper($this->removerAcentos($cliente['line2']));
                $cepEnt = $cliente['postcode'];
                $cidEnt = strtoupper($this->removerAcentos($cliente['city']));
                $estEnt = strtoupper($cliente['state_code']);

                $this->cdCli = $this->clienteDAO->inserirCliente($tipoCli, $this->deCli, $fantCli, $cnpj_cpfCli, $insc_rgCli, $ufEmisRG_IE, $cepCli, $endCli, $numCli, $complEndCli, $baiCli, $cidCli, $estCli, $codMunicipio, $dddCli, $foneCli, $emailCli, $endEnt, $comEnt, $baiEnt, $cepEnt, $cidEnt, $estEnt);

                $popupClienteCdastrado = true;
                if($popupClienteCdastrado) {
                  echo '<script>
                  Swal.fire({
                    icon: "success",
                    title: "Cliente cadastrado!",
                    html: "'.$this->cdCli.' - Cliente cadastrado com sucesso!<br><br>Cadastre o pedido!",
                    confirmButtonColor: "#039ddf",
                  })
                </script>';
                $popupClienteCdastrado = false;
                }
            }
        }

        // Métodos auxiliares
        // Método para separar apenas os dados do cliente
        private function lerDadosCliente($dadosCli){
            // Obtém os dados do cliente
            if(isset($dadosCli['customer'])){
                return $dadosCli['customer'];
            }else{
                return [];
            }
        }

        // Método para separar apenas os dados de endereço do cliente
        private function lerEnderecoEntrega($dadosEnd){
            // Obtém os dados do endereço
            if(isset($dadosEnd['shipping']['shipping_address'])){
                return $dadosEnd['shipping']['shipping_address'];
            }else{
                return [];
            }
        }

        // Método para concatenar os dados e o endereço do cliente num só objeto
        private function concatenarDadosCliente($dadosCli, $dadosEnd){
            // Concatena os dados do cliente e do endereço e os retorna
            return array_merge($dadosCli, $dadosEnd);
        }

        // Método que recebe o cpf para verificar se o cliente já tem cadastro no A7
        private function verificarClienteExistente($cliCpf){
            // Verifica se o cliente já existe no banco de dados
            return $this->clienteDAO->verificarSeClienteExiste($cliCpf);
        }

        // Método para validar o tipo de pessoa, se é Física ou Jurídica
        private function validarPF_PJ($cliente) {
            $tipoPessoa = $cliente['document_type'];

            switch($tipoPessoa){
                case 'CPF':
                    return 'F'; // Pessoa física
                    break;
                case 'CNPJ':
                    return 'J'; // Pessoa jurídica
                    break;
                default:
                    break;
            }
        }

        // Função para definir a partir da função validarPF_PJ, se o cliente é ISENTO
        private function definirIE_RG($tipoPessoa){
            switch($tipoPessoa){
                case 'F':
                    $insc_rgCli = 'ISENTO';
                    break;
                case 'J':
                    $insc_rgCli = 'ISENTO';
                    break;
                default:
                    break;
            }
            return $insc_rgCli;
        }

        // Função para remover caracteres especiais de uma string
        private function removerCaracteresEspeciais($str) {
            $novaStr = str_replace(['.', '-', '/'], '', $str);

            return $novaStr;
        }

        // Função para remover acentos de uma string
        private function removerAcentos($str) {
            $novaStr = preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/",
            "/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/","/(ç)/","/(Ç)/"),explode(" ","a A e E i I o O u U n N c C"), $str);

            return $novaStr;
        }

        // Métodos especiais
        // Construtor
        public function __construct(){
            // Cria-se uma instância da classe ClienteDAO, que é subclasse de ConexaoDAO. Essa instância chama os métodos específicos de ClienteDAO
            $this->clienteDAO = new ClienteDAO();
        }

        // Métodos de acesso
        // Método para acessar o código do cliente em outra classe
        public function getCodigoCliente() {
            return $this->cdCli;
        }

        // Método para acessar o nome do cliente em outra classe
        public function getNomeCliente() {
            return $this->deCli;
        }
    }
?>
