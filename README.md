# Integração Eskolare - Sistema A7 de Gestão

Este projeto visa criar uma integração entre a plataforma Eskolare e o Sistema A7 de gestão usando as linguagens PHP e JavaScript para automatizar o processo de recuperação de pedidos com pagamento aprovado, visando possibilitar o cadastro de clientes e emissão de notas fiscais no Sistema A7 de gestão e retorno dessas informações para a plataforma Eskolare.

## Pré-requisitos

#### Gerais
- Acesso à API da plataforma Eskolare.
- Acesso à base de dados do Sistema A7 de gestão.
- XAMPP Control Panel instalado.
- PHP instalado.

#### Sistema A7 de Gestão
- É essencial que os kits estejam previamente associados a uma escola. Essa associação é crucial para garantir a correta definição do kit correspondente, que é fornecido pela Eskolare.
- É imprescindível o cadastro do usuário 'ECOMMERCE'. Esse procedimento assegura a associação adequada dos pedidos a este usuário, garantindo, assim, o correto funcionamento da integração.
- É fundamental realizar o cadastro do tipo e condição de pagamento 'ECOMMERCE' para garantir a correta associação ao inserir o pedido no Sistema A7 de gestão.

## Configuração

1. Verificar se a porta 80 não está em uso, caso esteja pode-se mudar para 8080.

2. Caso o serviço do Apache não inicialize, verificar em Config no arquivo "php.ini" se a linha **extension=sqlsrv** está comentada, se estiver descomentar e reiniciar ou iniciar o serviço.

3. Se mesmo assim não funcionar, poderá baixar os drivers do PHP para SQLServer. Ao baixar o arquivo, terá que inserir na pasta: C:\xampp\php\ext\ os seguintes arquivos: [php_pdo_sqlsrv_82_ts_x64.dll](https://drive.google.com/file/d/15dJkTxrRLiRMp3hjGsZjZkpHXD5Khup8/view?usp=sharing) e [php_sqlsrv_82_ts_x64.dll](https://drive.google.com/file/d/1XakFrHrRGg55KfMFyTNOQj3BUuifRlOl/view?usp=sharing), ambos para a versão 8.2 do PHP.

4. Em seguida, no arquivo "php.ini" adicionar as linhas: **extension=php_pdo_sqlsrv_82_ts_x64.dll** e **php_sqlsrv_82_ts_x64.dll** e depois reiniciar ou iniciar o serviço do Apache.

5. Adicionalmente, é necessário configurar no Agendador de Tarefas do Windows o processo para a execução automática do script PHP. Este script realiza verificações a cada 10 minutos para identificar se houve a emissão de notas fiscais para os pedidos devidamente cadastrados via integração e efetivados no Sistema A7 de gestão.