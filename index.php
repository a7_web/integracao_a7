<?php
    require_once 'dao/ConexaoDAO.php';
    require_once 'dao/ClienteDAO.php';
    require_once 'dao/PedidoDAO.php';

    require_once 'core/Router.php';

    require_once 'model/Cliente.php';
    require_once 'model/Pedido.php';
    
    require_once 'controller/HomeController.php';
    require_once 'controller/ErroController.php';

    $template = file_get_contents('view/LayoutPadrao.html');

    ob_start();
        $router = new Router();
        $router->route();
        $saida = ob_get_contents();
    ob_end_clean();

    $templatePronto = str_replace('{{conteudo_dinamico}}', $saida, $template);
    echo $templatePronto;
?>
