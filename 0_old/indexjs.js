// -------------- Start: CREATE do Crud --------------
// function createUser() {
//     const name = document.getElementById('name').value
//     const cpf = document.getElementById('cpf').value
//     const endereco = document.getElementById('endereco').value
//     const telefone = document.getElementById('telefone').value
//     const email = document.getElementById('email').value

//     const form = new FormData()

//     form.append('name', name);
//     form.append('cpf', cpf);
//     form.append('endereco', endereco);
//     form.append('telefone', telefone);
//     form.append('email', email);

//     const url = 'http://localhost:8080/integracao_a7/cadastro.php'; //Url que será usada no fetch

//     //usando fetch API do javaScript para enviar as informações para o banco de dados
//     fetch(url,{
//         method: 'POST', //pode ser GET, PUT ou DELETE
//         body:form //Dados que serão enviados definidos acima
//     }).then(response =>{ //then É uma promisse, vamos passar o "response"
//         response.json().then(result =>{ //outra promisse com o "result". json porque é o formato da resposta que tá no arquivo cadastro.php
//             Swal.fire(result.success);
//             //limpa os campos
//             document.getElementById('name').value = "";
//             document.getElementById('cpf').value = "";
//             document.getElementById('endereco').value = "";
//             document.getElementById('telefone').value = "";
//             document.getElementById('email').value = "";
//         })
//     }).catch(err => console.log(err))

// }

// -------------- Start: Update do Crud --------------
/*function getId(id) {
    const form = new FormData();

form.append('id', id);
const url = 'http://localhost:8080/integracao_a7/get_id.php';

fetch(url, {
    method: 'POST',
    body: form
}).then(response => {
    response.json().then(data => {
        // console.log(data);
        window.localStorage.setItem('user',JSON.stringify(data));
        window.location.href="update.html";
    })
})
}

//-------------- Start: armazena os dados do usuário em cache
/*userData();
function userData(){
    const data = JSON.parse(localStorage.getItem('user'));
    const user = data[0];

    document.getElementById('id').value = user.id;
    document.getElementById('name-1').value = user.name;
    document.getElementById('cpf-1').value = user.cpf;
    document.getElementById('endereco-1').value = user.endereco;
    document.getElementById('telefone-1').value = user.telefone;
    document.getElementById('email-1').value = user.email;
}

function updateUser() {
    const id = document.getElementById('id').value;
    const name = document.getElementById('name-1').value;
    const cpf = document.getElementById('cpf-1').value;
    const endereco = document.getElementById('endereco-1').value;
    const telefone = document.getElementById('telefone-1').value;
    const email = document.getElementById('email-1').value;

    const form = new FormData();

    form.append('id', id);
    form.append('name', name);
    form.append('cpf', cpf);
    form.append('endereco', endereco);
    form.append('telefone', telefone);
    form.append('email', email);

    const url = 'http://localhost:8080/integracao_a7/update.php';

    fetch(url, {
        method: 'POST',
        body: form
    }).then(response => {
        response.json().then(data => {
            // console.log(data.message);
            Swal.fire(data.message).then(isConfirmed => {
                if(isConfirmed){
                    window.location.href="index.html";
                    window.localStorage.removeItem('user');
                }
            })
        })
    })
}

// -------------- Start: Delete do Crud --------------
function remove(id) {
    const form = new FormData();
    form.append('id', id);

    const url = 'http://localhost:8080/integracao_a7/remove.php';

    Swal.fire({
        title: 'Você tem certeza?',
        text: "Essa ação será irreversível!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim, delete it!',
        CancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.isConfirmed) {
            fetch(url, {
                method: 'POST',
                body: form
            }).then(response => {
                response.json().then(data => {
                    Swal.fire(data.message);
                    mostraDados();
                })//.setTimeout(() => {
                //     window.location.reload()
                //   }, 1000);
            }).catch(err => console.log(err))
        }
    })
}*/

// // Obtendo a URL atual
  // var urlAtual = window.location.href;
  // if (urlAtual.includes('cpf=')) {
  //   // Se o parâmetro "cpf" já existe na URL, remove o "cpf"
  //   urlAtual = urlAtual.replace(/(\?|&)cpf=[^&]*/, '');
  // }
  // // Construção da nova URL com o CPF
  // var novaUrl = urlAtual + '&cpf=' + cpf;
  // // Atualiza a URL da página
  // window.location.href = novaUrl;

  // ----------------------- 27/10/2023 ------------------------------
    // // Recupera os dados do localStorage
  // var pedidoData = localStorage.getItem('pedidoData');

  // console.log('Pedido:',pedidoData);

  // if (data) {
    // Objeto com os dados que serão enviados
    // var dadosCliente = { pedidoData: pedidoData };

  //   var url = '?pagina=home&metodo=enviar-dados-para-php';
  //   var requestOptions = {
  //     method: 'POST',
  //     headers: { 'Content-Type': 'application/json' },
  //     body: JSON.stringify(dadosCliente)
  //   };

  //   fetch(url, requestOptions)
  //     // .then(function (response) {
  //       .then(response =>{
  //       if (response.ok) {
  //         console.log('response:',response);
  //         return response.text(); // ou response.json() se a resposta for em JSON
  //       } else {
  //         throw new Error('Erro na resposta do servidor!');
  //       }
  //     })
  //     .then(function (data) {
  //       console.log('Resposta do PHP:', data);
  //     })
  //     .catch(function (error) {
  //       console.error('Erro ao enviar os dados para o PHP', error);
  //     });
  // } else {
  //   console.error('Dados do pedido não encontrados no localStorage!');
  // }

//   function verificarCliente(pedido) {
//     var numFormatado = pedido + '-01'
//     var url = `https://api.eskolare.com/api/integrations/eskolare/orders/${numFormatado}/`;
//     var authToken = '9sqr3m80ezfMBWin3UJIrWiZmqBX1OtG9JpuYXIqMhaQTrZrDe';
//     var myHeaders = new Headers();
//     myHeaders.append("Authorization", `Bearer ${authToken}`);

//     var requestOptions = {
//       method: 'GET',
//       headers: myHeaders,
//       redirect: 'follow'
//     };

//     fetch(url, requestOptions)
//       .then(response => response.json())
//       .then(data => {
//         var dadosCliente = {
//           pessoa: data.customer.document_type,
//           nome: data.customer.first_name + ' ' + data.customer.last_name_name,
//           cpf_cnpj: data.customer.document,
//           ie_rg: 'ISENTO',
//           uf_emissao: data.shipping.shipping_address.state_code,
//         }
//       })
//       .catch(error => {
//         console.error('Erro ao buscar dados:', error);
//       });
//   }

//   <?php
//     class HomeController{
//         public function index(/*$params*/){
//             // Chama a Home.html para mostrar no conteudo dinamico do layout padrão
//             echo file_get_contents('view/Home.html');
//         }

//         public function receberDadosAPI(){
//             if(isset($_POST['pedidoData'])){
//                 // Recebe os dados enviados via POST
//                 $pedidoData = $_POST['pedidoData'];

//                 var_dump($pedidoData);

//                 echo 'Dados recebidos no controller!';
//             }else{
//                 echo 'Chave "pedidoData" não encontrada nos dados do POST!';
//             }
//         }
//     }
// ?>

