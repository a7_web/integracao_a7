<?php 
    class EventLoop{
        protected array $callStack = [];

        public function defer(callable $callable): void{
            $this->callStack[] = new Fiber($callable);
        }

        public function run(){
            while($this->callStack !== []){
                foreach($this->callStack as $id => $fiber){
                    $this->callFiber($id, $fiber);
                }
            }
        }

        public function next(mixed $value = null){
            return \Fiber::suspend($value);
        }

        public function sleep(float $seconds){
            // \sleep($seconds);

            $stop = microtime(true) + $seconds;

            while(microtime(true) < $stop){
                $this->next();
            }
        }

        protected function callFiber(int $id, \Fiber $fiber): void{
            if(!$fiber->isStarted()){
                $fiber->start($id);
            }elseif(!$fiber->isTerminated()){
                $fiber->resume();
            }else{
                unset($this->callStack[$id]);

                $fiber->getReturn();
            }
        }
    }
?>