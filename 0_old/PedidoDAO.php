<?php
    // Partes de código que não serão mais usados, mas que podem ser úteis para consulta
    // Método para verificar se a escola(instituição) do pedido existe no banco de dados, retornar true ou false e o código da escola
        // public function verificarSeEscolaExiste($cnpjInst){
        //     $sql = "SELECT COUNT(*) AS count, cod_esc FROM t_esc WHERE cnpj = ? GROUP BY cod_esc";
        //     $params = array($cnpjInst);

        //     $stmt = sqlsrv_query($this->db->getConnection(), $sql, $params);

        //     if ($stmt === false) {
        //         // Lança uma exceção em caso de erro
        //         throw new Exception(print_r(sqlsrv_errors(), true));
        //     }

        //     $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);

        //     if ($row !== null) {
        //         $count = isset($row['count']) ? $row['count'] : 0;
        //         $cod_esc = isset($row['cod_esc']) ? $row['cod_esc'] : null;
        //     } else {
        //         // Caso em que $row é nulo
        //         $count = 0;
        //         $cod_esc = null;
        //     }
  
        //     $escolaExiste = $count > 0;
  
        //     return array(
        //         'escolaExiste' => $escolaExiste,
        //         'cod_esc' => $cod_esc
        //     );
        // }

        // Método para verificar se o item do pedido existe no banco de dados, retornar o código dos itens
        // public function verificarSeItemExiste($itensRef){
        //     $sql = "SELECT cdItem FROM t_itens WHERE referencia IN (" . implode(",", array_fill(0, count($itensRef), "?")) . ")";

        //     $params = $itensRef;

        //     $stmt = sqlsrv_query($this->db->getConnection(), $sql, $params);

        //     if ($stmt === false) {
        //         // Lança uma exceção em caso de erro
        //         throw new Exception(print_r(sqlsrv_errors(), true));
        //     }

        //     $results = [];

        //     while($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)){
        //         $results[] = $row;
        //     }

        //     return $results;
        // }

        // Método para verificar se o Kit existe no banco de dados, retornar o codigo do Kit
        // public function verificarSeKitExiste($codEsc, $cdItens){
        //     $sql = "SELECT DISTINCT k.cod_auto AS codigoKit, k.qtd_item AS qtdItem
        //     FROM t_kit AS k
        //     INNER JOIN t_kit_item AS ki ON k.cod_auto = ki.cod_kit
        //     WHERE k.ativo = 'S' AND k.cod_esc = ? AND ki.cod_item IN (" . implode(",", array_fill(0, count($cdItens), "?")) . ")";

        //     $params = array_merge([$codEsc], array_column($cdItens, 'cdItem'));

        //     $stmt = sqlsrv_query($this->db->getConnection(), $sql, $params);

        //     if ($stmt === false) {
        //         // Lança uma exceção em caso de erro
        //         throw new Exception(print_r(sqlsrv_errors(), true));
        //     }

        //     $row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);

        //     if ($row !== null) {
        //         $codKit = isset($row['codigoKit']) ? $row['codigoKit'] : null;
        //         $qtdItem = isset($row['qtdItem']) ? $row['qtdItem'] : null;
        //     } else {
        //         // Caso em que $row é nulo
        //         $codKit = 0;
        //         $qtdItem = 0;
        //     }
  
        //     return array(
        //         'codKit' => $codKit,
        //         'qtdItem' => $qtdItem
        //     );

        //     // if($row){
        //     //     return $row['codigoKit'];
        //     // }else{
        //     //     return null;
        //     // }
        // }
?>