<?php
$serverName = "192.168.10.60";
$connectionOptions = array(
    "Database" => "A7_FTD",
    "Uid" => "a7",
    "PWD" => "sql#Hais23"
);

$conn = sqlsrv_connect($serverName, $connectionOptions);

if (!$conn) {
    die("Erro na conexão com o banco de dados: " . print_r(sqlsrv_errors(), true));
}

// Receba os dados enviados via POST
$pessoa = $_POST['pessoa'];
$nome = $_POST['nome'];
$cpf_cnpj = $_POST['cpf_cnpj'];

// Prepare a consulta SQL de inserção
$sql = "INSERT INTO t_cli (pessoa, nome, cpf_cnpj) VALUES (?, ?, ?)";
$params = array($pessoa, $nome, $cpf_cnpj);

$stmt = sqlsrv_query($conn, $sql, $params);

// Após a inserção no banco de dados
if ($stmt === false) {
  die(print_r(sqlsrv_errors(), true));
} else {
  $message = ["message" => "Dados do cliente cadastrados com sucesso"];
  header('Content-Type: application/json');
  http_response_code(200);
  echo json_encode($message);
}

sqlsrv_close($conn);

?>
