<?php
  class HomeController{
    public function index($params){
      // Chama a Home.html para mostrar no conteudo dinamico do layout padrão
      echo file_get_contents('view/Home.html');

      if(isset($params)){
        // Inicializa o cURL para chamar a API dos detalhes do pedido
        $curl = curl_init();

        curl_setopt_array($curl, array(
          // CURLOPT_URL => 'https://api.eskolare-development.com/api/integrations/eskolare/orders/'.$params.'/', // DESENVOLVIMENTO
          CURLOPT_URL => 'https://api.eskolare.com/api/integrations/eskolare/orders/'.$params.'/', // PRODUÇÃO
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          // CURLOPT_HTTPHEADER => array(
          //   'Authorization: Bearer 9sqr3m80ezfMBWin3UJIrWiZmqBX1OtG9JpuYXIqMhaQTrZrDe', // DESENVOLVIMENTO
          // ),
          CURLOPT_HTTPHEADER => array(
              'Authorization: Bearer XbX1N51LUr4uQv4uaG3b1Ym41gYoyWp7ZjnytWx0aQ8Zo2FCyJ', // PRODUÇÃO
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        // Obtém os dados da API e converte em objeto, para que as respectivas classes possam manipular os dados
        $dadosApi = $this->converterJsonParaObjeto($response);

        // Instância da classe Cliente para enviar a API como objeto
        $cliente = new Cliente();
        $cliente->receberDadosApi($dadosApi);

        // Instância da classe Pedido para enviar a API como objeto
        $pedido = new Pedido($cliente);
        $pedido->receberDadosApi($dadosApi);
      }
    }

    private function converterJsonParaObjeto($response){
      $dados = json_decode($response, true);

      if($dados !== null){
        return $dados;
      }else{
        return [];
      }
    }
  }
?>
