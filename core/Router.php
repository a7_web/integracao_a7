<?php
    class Router {
        public function route() {
            // Obtém os parâmetros da URL
            $urlGet = $_GET;

            // Verifica se está sendo passado algum método pela URL
            if(isset($urlGet['metodo'])){
                $actionName = $urlGet['metodo'];
            }else{
                $actionName = 'index';
            }

            // Verifica se há um controlador na URL
            if (isset($urlGet['pagina'])) {
                $controllerName = ucfirst($urlGet['pagina']) . 'Controller';
            }else{
                $controllerName = 'HomeController';
            }

            // Verifica se o controlador especificado existe, caso não, mostra página de erro
            if(!class_exists($controllerName)){
                $controllerName = 'ErroController';
            }

            // Faz a leitura da URL para verificar se há o número do pedido
            if(isset($urlGet['numPedido']) && $urlGet['numPedido'] != null){
                $numPedido = $urlGet['numPedido'];
            }else{
                $numPedido = null;
            }

            call_user_func_array(array(new $controllerName, $actionName), array($numPedido));
        }
    }
?>
